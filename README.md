# Blueprint Hub

Blueprint's server-side component and configuration manager.

## Hub Functionality
* Modular and extendible configuration management for on-device settings
* Kerberos and RADIUS authentication
* LDAP interface for user management
* DNS server management

## Branch Functionality
* Configuration management
* DNS caching
* Kerberos authentication point
